class Instruction:
    in_state = None
    in_word = None

    out_state = None
    out_word = None
    direction = None

    def __init__(self, in_state, in_word, out_state, out_word, direction):
        self.in_state = in_state
        self.in_word = in_word
        self.out_state = out_state
        self.out_word = out_word
        self.direction = direction

    def matches(self, state, word):
        return state == self.in_state and word == self.in_word

    def __str__(self):
        return str(self.in_state) + '-' + str(self.in_word) + '/' + str(self.out_word) + ',' + str(
            self.direction) + '-' + str(self.out_state)

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def from_str(s):
        """
        Parses a string in format inState-inWord/outWord,direction-outState
        :param string s:
        :return:
        """
        split = s.split('-')
        t = split[1].split('/')
        t2 = t[1].split(',')

        _instr = Instruction(split[0], t[0], split[2], t2[0], t2[1])

        return _instr


class TuringMachine:
    instructions = None

    current_index = 0
    current_state = None
    band = dict()

    is_finished = False

    success_states = None

    step_count = 0

    def __init__(self, instructions, start_state, word, success_states):
        """
        :param Instruction[] instructions:
        :param string start_state:
        :param string word:
        """
        self.instructions = instructions
        self.current_state = start_state

        i = self.current_index
        for c in word:
            self.band[i] = c
            i += 1

        self.success_states = success_states

        def no(*args):
            pass

        self.on_pre_step = no
        self.on_post_step = no

    def step(self):
        instruction = self.find_current_instruction()

        self.on_pre_step(instruction)

        if instruction is None:
            self.is_finished = True
            return

        self.band[self.current_index] = instruction.out_word
        self.current_state = instruction.out_state

        if instruction.direction == 'L':
            self.current_index -= 1
        else:
            self.current_index += 1

        self.step_count += 1
        self.on_post_step(instruction)

    def run(self):
        while not self.is_finished:
            self.step()

    def find_current_instruction(self):
        for instruction in self.instructions:
            if instruction.matches(self.current_state, self.band.setdefault(self.current_index, '_')):
                return instruction

        return None

    def is_success(self):
        return self.current_state in self.success_states

    def band_str(self, show_state=True):
        s = ''
        for k in sorted(self.band):
            v = self.band[k]
            if k == self.current_index and show_state:
                s += '(' + self.current_state + ')'
            s += str(v)

        return s


conf = '''
q0
q4
q0-0/X,R-q1
q1-0/0,R-q1
q1-Y/Y,R-q1
q1-1/Y,L-q2
q2-0/0,L-q2
q2-Y/Y,L-q2
q2-X/X,R-q0
q0-Y/Y,R-q3
q3-Y/Y,R-q3
q3-_/_,R-q4
'''.strip().split('\n')

conf = '''
q0
q11
q0-x/x,R-q1
q1-x/x,R-q2
q1-_/_,L-q8
q2-x/x,R-q2
q2-_/_,L-q3
q2-t/t,L-q3
q3-x/t,L-q4
q4-x/x,L-q4
q4-_/_,R-q5
q4-t/t,R-q5
q5-x/t,R-q6
q6-x/x,R-q7
q7-x/x,L-q2
q7-t/t,L-q8
q8-x/1,R-q9
q9-t/t,R-q9
q9-_/_,L-q10
q10-1/1,L-q10
q10-t/x,L-q10
q10-_/_,R-q11
'''.strip().split('\n')

conf = '''
q0
q6
q0-a/a,R-q0
q0-b/b,R-q0
q0-_/x,L-q1
q1-x/x,L-q1
q1-b/x,R-q2
q2-x/x,R-q2
q2-a/a,R-q2
q2-b/b,R-q2
q2-_/b,L-q3
q3-x/x,L-q1
q3-a/a,L-q3
q3-b/b,L-q3
q1-a/x,R-q4
q4-x/x,R-q4
q4-a/a,R-q4
q4-b/b,R-q4
q4-_/a,L-q3
q1-_/_,R-q5
q5-x/_,R-q5
q5-a/a,R-q6
q5-b/b,R-q6
q5-_/_,R-q6
'''.strip().split('\n')

instr = list()
for instr_txt in conf[2:]:
    instr.append(Instruction.from_str(instr_txt))

print(instr)

tm = TuringMachine(instr, conf[0], 'aaabbbab', conf[1])


def get_tex_band_str(tm):
    s = ''
    for k in sorted(tm.band):
        v = tm.band[k]
        if k == tm.current_index:
            s += tm.current_state[0] + '_{' + tm.current_state[1:] + "}"
        if str(v) is '_':
            s += '\_'
        else:
            s += str(v)

    return s.strip('\_')


def print_step(instr):
    print(tm.band_str(), instr)
   # print(get_tex_band_str(tm), '\T')


tm.on_pre_step = print_step

tm.run()

print('Band Result:', tm.band_str(False))
print('Accepted:', tm.is_success())
